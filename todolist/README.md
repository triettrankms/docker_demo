#This is the front end of the project

#Install

`npm install`

#Start

`npm start`

**Note** It is better to make sure this project is started after back end has been up and running

#Config if needed

1. The file `config.js` has several configurations that need to be manually configured so that they are insync with the back end project
2. The default port is `3000`. To change the default port, try the solutions [here](https://github.com/facebook/create-react-app/issues/1083)

#Neccessary packages

The necessary packages are described in the package.json file. They are installed automatically when running the command `npm install`
