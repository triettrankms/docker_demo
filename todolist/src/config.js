const config = {
  BACK_END_HOST: 'localhost',
  BACK_END_PORT: '3000',
  FRONT_END_HOST: 'localhost',
  FRONT_END_PORT: '80'
};

export default config;
