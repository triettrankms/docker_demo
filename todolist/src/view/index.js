import { Button, Table, Form, FormGroup, Label, Input } from "reactstrap";
import React, { Component } from "react";
import api from "../api/";
export default class HomePage extends Component {
  //debug
  constructor() {
    super();
    this.onSubmit = this.onSubmit.bind(this);
    this.state = {
      task: "",
      name: "",
      currentList: { list: [] }
    };
  }

  async onSubmit(e) {
    e.preventDefault();
    const postSuccessfully = await api.postTodo(
      this.state.name,
      this.state.task
    );
    const newList = this.state.currentList.list;
    newList.push({ name: this.state.name, task: this.state.task });
    postSuccessfully &&
      this.setState({ currentList: Object.assign({}, this.state.currentList) });
    this.setState({ name: "", task: "" });
  }

  async componentDidMount() {
    const newList = await api.getTodo();
    newList &&
      this.setState({ currentList: Object.assign({}, { list: newList }) });
  }
  render() {
    const mockObj = {
      list: [
        {
          name: "John Newman",
          task: "Finish drawing last frame of the previous story "
        },
        {
          name: "John Newman",
          task: "Finish drawing last frame of the previous story "
        },
        {
          name: "John Newman",
          task: "Finish drawing last frame of the previous story "
        }
      ]
    };
    const { list } = this.state.currentList;
    console.log(list);
    return (
      <div>
        <Form inline onSubmit={this.onSubmit}>
          <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
            <Label for="name" className="mr-sm-2">
              Name
            </Label>
            <Input
              type="text"
              name="email"
              id="name"
              placeholder="Michael Jackson"
              value={this.state.name}
              onChange={e => this.setState({ name: e.target.value })}
            />
          </FormGroup>
          <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
            <Label for="task" className="mr-sm-2">
              Task
            </Label>
            <Input
              type="text"
              name="task"
              id="task"
              placeholder="Doing the dishes"
              value={this.state.task}
              onChange={e => this.setState({ task: e.target.value })}
            />
          </FormGroup>
          <Button>Submit</Button>
        </Form>
        <Table>
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Task</th>
            </tr>
          </thead>
          <tbody>
            {list.map((item, index) => (
              <tr key={index}>
                <td>{index + 1}</td>
                <td>{item.name}</td>
                <td>{item.task}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
    );
  }
}
