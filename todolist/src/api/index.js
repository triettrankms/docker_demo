import axios from "axios";
import config from "../config";

const BACK_END_BASE_URL = `http://${config.BACK_END_HOST}:${
  config.BACK_END_PORT
}`;

const api = {
  postTodo: async (name, task) =>
    await axios
      .post(`${BACK_END_BASE_URL}/todo`, { name, task })
      .then(function(response) {
        if (response.status === 200) return true;
        return false;
      }),

  getTodo: async () => {
    let returnVal = await axios
      .get(`${BACK_END_BASE_URL}/todo`)
      .then(function(response) {
        if (response.status === 200) return response.data;
        return false;
      });
    return returnVal;
  }
};

export default api;
