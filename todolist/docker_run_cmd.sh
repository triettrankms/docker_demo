#!/bin/bash

#This script invokes the docker command to run the todlist_fe container with specific hard-coded settings

#Global Variables

HOST_PORT_BE="$1"

if [[ -z "$HOST_PORT_BE"  ]]; then
	docker run --network="todolist" --ip="10.1.5.3" --name="todolist_fe" -p 80:80 minhtrietbkit/todolist_fe:latest
else 
	docker run --network="todolist" --ip="10.1.5.3" --name="todolist_fe" -p 80:80 minhtrietbkit/todolist_fe:latest "$HOST_PORT_BE"
fi

