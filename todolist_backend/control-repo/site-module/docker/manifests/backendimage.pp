class docker::backendimage {
  docker::image { 'linda/backend':
   ensure => 'latest',
  }

  docker::run { 'linda/backend':
   image   => 'linda/backend',
   #ensure => absent,
   command => 'bla bla bla',
   expose  => ['3000', '3000'],
   env     => ['FOO=BAR', 'FOO2=BAR2'],
   
  }
}
