#!/bin/bash

#This script will start the backend specified back-end host/port and database host/port

#INPUT
#back-end host,port: <host>,<port>
#db host,port: <host>,<port>

#Default back-end host is 'localhost'
#Default back-end port is '3000'

#Default db host is 'localhost'
#Default db port is '27017'


#Gloval vars
BACK_END_DEFAULT_HOST="localhost"
BACK_END_DEFAULT_PORT="3000"
DB_DEFAULT_HOST="localhost"
DB_DEFAULT_PORT="27017"

BACK_END_HOST=""
BACK_END_PORT=""
DB_HOST=""
DB_PORT=""


while getopts ":d:b:" opt; do
  case $opt in
    b)
	  read -r BACK_END_HOST BACK_END_PORT <<< $( echo "$OPTARG" | tr "," " " )
      ;;
	d)
	  read -r DB_HOST DB_PORT <<< $( echo "$OPTARG" | tr "," " " )
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

if [[ -z "$BACK_END_HOST"  ]]; then
	echo Back-end Host input is null
	echo Use default host: "$BACK_END_DEFAULT_HOST"
else
	echo Back-end Host input is "$BACK_END_HOST"
fi

if [[ -z "$BACK_END_PORT"  ]]; then
	echo Back-end Port input is null
	echo Use default port: "$BACK_END_DEFAULT_PORT"
else
	echo Back-end Port input is "$BACK_END_PORT"
fi

BACK_END_HOST=${BACK_END_HOST:="$BACK_END_DEFAULT_HOST"}
BACK_END_PORT=${BACK_END_PORT:="$BACK_END_DEFAULT_PORT"}

if [[ -z "$DB_HOST"  ]]; then
	echo DB Host input is null
	echo Use default host: "$DB_DEFAULT_HOST"
else
	echo DB Host input is "$DB_HOST"
fi

if [[ -z "$DB_PORT"  ]]; then
	echo DB Port input is null
	echo Use default port: "$DB_DEFAULT_PORT"
else
	echo DB Port input is "$DB_PORT"
fi

DB_HOST=${DB_HOST:="$DB_DEFAULT_HOST"}
DB_PORT=${DB_PORT:="$DB_DEFAULT_PORT"}

sed -ri "/BACK_END_HOST/s/'.*'/'$BACK_END_HOST'/" config.js
sed -ri "/BACK_END_PORT/s/'.*'/'$BACK_END_PORT'/" config.js

sed -ri "/DB_HOST/s/'.*'/'$DB_HOST'/" config.js
sed -ri "/DB_PORT/s/'.*'/'$DB_PORT'/" config.js

#echo Restart mongod service to close previous connection if necessary. Please enter sudo password
#sudo service mongod restart

echo Starting server
echo Below is server log
echo --------------------
nodejs app.js

