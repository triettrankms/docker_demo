#!/bin/bash
wget 'https://apt.puppetlabs.com/puppet5-release-bionic.deb'
sudo dpkg -i puppet5-release-bionic.deb
sudo apt-get update -y
sudo apt-get install puppetserver -y
sudo systemctl start puppetserver