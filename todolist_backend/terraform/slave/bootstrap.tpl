#!/bin/bash
wget 'https://apt.puppetlabs.com/puppet5-release-bionic.deb'
sudo dpkg -i puppet5-release-bionic.deb
sudo apt-get update -y
sudo apt-get install puppet-agent -y
#sudo sed -i '$ a [main]' /etc/puppetlabs/puppet/puppet.conf
sudo sed -i '$ a [main]\nserver = ${master_hostname_script}' /etc/puppetlabs/puppet/puppet.conf
sudo /opt/puppetlabs/bin/puppet resource service puppet ensure=running enable=true