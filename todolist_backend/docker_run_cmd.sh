#!/bin/bash

#This script wraps docker command to run the todolist_be container with specific hard-coded setting
docker run --network="todolist" --ip="10.1.5.2" --name="todolist_be" -p 3000:3000 minhtrietbkit/todolist_be:latest
